// **********************************************************************
//
// Copyright (c) 2003-2018 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.6.5
//
// <auto-generated>
//
// Generated from file `Router.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

#ifndef ICE_API_EXPORTS
#   define ICE_API_EXPORTS
#endif
#include <Ice/Router.h>
#include <IceUtil/PushDisableWarnings.h>
#include <Ice/LocalException.h>
#include <Ice/ObjectFactory.h>
#include <Ice/Outgoing.h>
#include <Ice/OutgoingAsync.h>
#include <Ice/BasicStream.h>
#include <IceUtil/Iterator.h>
#include <IceUtil/PopDisableWarnings.h>

#ifndef ICE_IGNORE_VERSION
#   if ICE_INT_VERSION / 100 != 306
#       error Ice version mismatch!
#   endif
#   if ICE_INT_VERSION % 100 > 50
#       error Beta header file detected
#   endif
#   if ICE_INT_VERSION % 100 < 5
#       error Ice patch level mismatch!
#   endif
#endif

namespace
{

const ::std::string __Ice__Router__getClientProxy_name = "getClientProxy";

const ::std::string __Ice__Router__getServerProxy_name = "getServerProxy";

const ::std::string __Ice__Router__addProxies_name = "addProxies";

const ::std::string __Ice__RouterFinder__getRouter_name = "getRouter";

}
#ifdef __SUNPRO_CC
class ICE_API IceProxy::Ice::Router;
#endif
ICE_API ::IceProxy::Ice::Object* ::IceProxy::Ice::upCast(::IceProxy::Ice::Router* p) { return p; }

void
::IceProxy::Ice::__read(::IceInternal::BasicStream* __is, ::IceInternal::ProxyHandle< ::IceProxy::Ice::Router>& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::Ice::Router;
        v->__copyFrom(proxy);
    }
}

::Ice::ObjectPrx
IceProxy::Ice::Router::getClientProxy(const ::Ice::Context* __ctx)
{
    __checkTwowayOnly(__Ice__Router__getClientProxy_name);
    ::IceInternal::Outgoing __og(this, __Ice__Router__getClientProxy_name, ::Ice::Nonmutating, __ctx);
    __og.writeEmptyParams();
    if(!__og.invoke())
    {
        try
        {
            __og.throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
            throw __uue;
        }
    }
    ::Ice::ObjectPrx __ret;
    ::IceInternal::BasicStream* __is = __og.startReadParams();
    __is->read(__ret);
    __og.endReadParams();
    return __ret;
}

::Ice::AsyncResultPtr
IceProxy::Ice::Router::begin_getClientProxy(const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    __checkAsyncTwowayOnly(__Ice__Router__getClientProxy_name);
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __Ice__Router__getClientProxy_name, __del, __cookie);
    try
    {
        __result->prepare(__Ice__Router__getClientProxy_name, ::Ice::Nonmutating, __ctx);
        __result->writeEmptyParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

#ifdef ICE_CPP11

::Ice::AsyncResultPtr
IceProxy::Ice::Router::__begin_getClientProxy(const ::Ice::Context* __ctx, const ::IceInternal::Function<void (const ::Ice::ObjectPrx&)>& __response, const ::IceInternal::Function<void (const ::Ice::Exception&)>& __exception, const ::IceInternal::Function<void (bool)>& __sent)
{
    class Cpp11CB : public ::IceInternal::Cpp11FnCallbackNC
    {
    public:

        Cpp11CB(const ::std::function<void (const ::Ice::ObjectPrx&)>& responseFunc, const ::std::function<void (const ::Ice::Exception&)>& exceptionFunc, const ::std::function<void (bool)>& sentFunc) :
            ::IceInternal::Cpp11FnCallbackNC(exceptionFunc, sentFunc),
            _response(responseFunc)
        {
            CallbackBase::checkCallback(true, responseFunc || exceptionFunc != nullptr);
        }

        virtual void completed(const ::Ice::AsyncResultPtr& __result) const
        {
            ::Ice::RouterPrx __proxy = ::Ice::RouterPrx::uncheckedCast(__result->getProxy());
            ::Ice::ObjectPrx __ret;
            try
            {
                __ret = __proxy->end_getClientProxy(__result);
            }
            catch(const ::Ice::Exception& ex)
            {
                Cpp11FnCallbackNC::exception(__result, ex);
                return;
            }
            if(_response != nullptr)
            {
                _response(__ret);
            }
        }
    
    private:
        
        ::std::function<void (const ::Ice::ObjectPrx&)> _response;
    };
    return begin_getClientProxy(__ctx, new Cpp11CB(__response, __exception, __sent));
}
#endif

::Ice::ObjectPrx
IceProxy::Ice::Router::end_getClientProxy(const ::Ice::AsyncResultPtr& __result)
{
    ::Ice::AsyncResult::__check(__result, this, __Ice__Router__getClientProxy_name);
    ::Ice::ObjectPrx __ret;
    if(!__result->__wait())
    {
        try
        {
            __result->__throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            throw ::Ice::UnknownUserException(__FILE__, __LINE__, __ex.ice_name());
        }
    }
    ::IceInternal::BasicStream* __is = __result->__startReadParams();
    __is->read(__ret);
    __result->__endReadParams();
    return __ret;
}

::Ice::ObjectPrx
IceProxy::Ice::Router::getServerProxy(const ::Ice::Context* __ctx)
{
    __checkTwowayOnly(__Ice__Router__getServerProxy_name);
    ::IceInternal::Outgoing __og(this, __Ice__Router__getServerProxy_name, ::Ice::Nonmutating, __ctx);
    __og.writeEmptyParams();
    if(!__og.invoke())
    {
        try
        {
            __og.throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
            throw __uue;
        }
    }
    ::Ice::ObjectPrx __ret;
    ::IceInternal::BasicStream* __is = __og.startReadParams();
    __is->read(__ret);
    __og.endReadParams();
    return __ret;
}

::Ice::AsyncResultPtr
IceProxy::Ice::Router::begin_getServerProxy(const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    __checkAsyncTwowayOnly(__Ice__Router__getServerProxy_name);
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __Ice__Router__getServerProxy_name, __del, __cookie);
    try
    {
        __result->prepare(__Ice__Router__getServerProxy_name, ::Ice::Nonmutating, __ctx);
        __result->writeEmptyParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

#ifdef ICE_CPP11

::Ice::AsyncResultPtr
IceProxy::Ice::Router::__begin_getServerProxy(const ::Ice::Context* __ctx, const ::IceInternal::Function<void (const ::Ice::ObjectPrx&)>& __response, const ::IceInternal::Function<void (const ::Ice::Exception&)>& __exception, const ::IceInternal::Function<void (bool)>& __sent)
{
    class Cpp11CB : public ::IceInternal::Cpp11FnCallbackNC
    {
    public:

        Cpp11CB(const ::std::function<void (const ::Ice::ObjectPrx&)>& responseFunc, const ::std::function<void (const ::Ice::Exception&)>& exceptionFunc, const ::std::function<void (bool)>& sentFunc) :
            ::IceInternal::Cpp11FnCallbackNC(exceptionFunc, sentFunc),
            _response(responseFunc)
        {
            CallbackBase::checkCallback(true, responseFunc || exceptionFunc != nullptr);
        }

        virtual void completed(const ::Ice::AsyncResultPtr& __result) const
        {
            ::Ice::RouterPrx __proxy = ::Ice::RouterPrx::uncheckedCast(__result->getProxy());
            ::Ice::ObjectPrx __ret;
            try
            {
                __ret = __proxy->end_getServerProxy(__result);
            }
            catch(const ::Ice::Exception& ex)
            {
                Cpp11FnCallbackNC::exception(__result, ex);
                return;
            }
            if(_response != nullptr)
            {
                _response(__ret);
            }
        }
    
    private:
        
        ::std::function<void (const ::Ice::ObjectPrx&)> _response;
    };
    return begin_getServerProxy(__ctx, new Cpp11CB(__response, __exception, __sent));
}
#endif

::Ice::ObjectPrx
IceProxy::Ice::Router::end_getServerProxy(const ::Ice::AsyncResultPtr& __result)
{
    ::Ice::AsyncResult::__check(__result, this, __Ice__Router__getServerProxy_name);
    ::Ice::ObjectPrx __ret;
    if(!__result->__wait())
    {
        try
        {
            __result->__throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            throw ::Ice::UnknownUserException(__FILE__, __LINE__, __ex.ice_name());
        }
    }
    ::IceInternal::BasicStream* __is = __result->__startReadParams();
    __is->read(__ret);
    __result->__endReadParams();
    return __ret;
}

::Ice::ObjectProxySeq
IceProxy::Ice::Router::addProxies(const ::Ice::ObjectProxySeq& __p_proxies, const ::Ice::Context* __ctx)
{
    __checkTwowayOnly(__Ice__Router__addProxies_name);
    ::IceInternal::Outgoing __og(this, __Ice__Router__addProxies_name, ::Ice::Idempotent, __ctx);
    try
    {
        ::IceInternal::BasicStream* __os = __og.startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_proxies);
        __og.endWriteParams();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    if(!__og.invoke())
    {
        try
        {
            __og.throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
            throw __uue;
        }
    }
    ::Ice::ObjectProxySeq __ret;
    ::IceInternal::BasicStream* __is = __og.startReadParams();
    __is->read(__ret);
    __og.endReadParams();
    return __ret;
}

::Ice::AsyncResultPtr
IceProxy::Ice::Router::begin_addProxies(const ::Ice::ObjectProxySeq& __p_proxies, const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    __checkAsyncTwowayOnly(__Ice__Router__addProxies_name);
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __Ice__Router__addProxies_name, __del, __cookie);
    try
    {
        __result->prepare(__Ice__Router__addProxies_name, ::Ice::Idempotent, __ctx);
        ::IceInternal::BasicStream* __os = __result->startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_proxies);
        __result->endWriteParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

#ifdef ICE_CPP11

::Ice::AsyncResultPtr
IceProxy::Ice::Router::__begin_addProxies(const ::Ice::ObjectProxySeq& __p_proxies, const ::Ice::Context* __ctx, const ::IceInternal::Function<void (const ::Ice::ObjectProxySeq&)>& __response, const ::IceInternal::Function<void (const ::Ice::Exception&)>& __exception, const ::IceInternal::Function<void (bool)>& __sent)
{
    class Cpp11CB : public ::IceInternal::Cpp11FnCallbackNC
    {
    public:

        Cpp11CB(const ::std::function<void (const ::Ice::ObjectProxySeq&)>& responseFunc, const ::std::function<void (const ::Ice::Exception&)>& exceptionFunc, const ::std::function<void (bool)>& sentFunc) :
            ::IceInternal::Cpp11FnCallbackNC(exceptionFunc, sentFunc),
            _response(responseFunc)
        {
            CallbackBase::checkCallback(true, responseFunc || exceptionFunc != nullptr);
        }

        virtual void completed(const ::Ice::AsyncResultPtr& __result) const
        {
            ::Ice::RouterPrx __proxy = ::Ice::RouterPrx::uncheckedCast(__result->getProxy());
            ::Ice::ObjectProxySeq __ret;
            try
            {
                __ret = __proxy->end_addProxies(__result);
            }
            catch(const ::Ice::Exception& ex)
            {
                Cpp11FnCallbackNC::exception(__result, ex);
                return;
            }
            if(_response != nullptr)
            {
                _response(__ret);
            }
        }
    
    private:
        
        ::std::function<void (const ::Ice::ObjectProxySeq&)> _response;
    };
    return begin_addProxies(__p_proxies, __ctx, new Cpp11CB(__response, __exception, __sent));
}
#endif

::Ice::ObjectProxySeq
IceProxy::Ice::Router::end_addProxies(const ::Ice::AsyncResultPtr& __result)
{
    ::Ice::AsyncResult::__check(__result, this, __Ice__Router__addProxies_name);
    ::Ice::ObjectProxySeq __ret;
    if(!__result->__wait())
    {
        try
        {
            __result->__throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            throw ::Ice::UnknownUserException(__FILE__, __LINE__, __ex.ice_name());
        }
    }
    ::IceInternal::BasicStream* __is = __result->__startReadParams();
    __is->read(__ret);
    __result->__endReadParams();
    return __ret;
}

const ::std::string&
IceProxy::Ice::Router::ice_staticId()
{
    return ::Ice::Router::ice_staticId();
}

::IceProxy::Ice::Object*
IceProxy::Ice::Router::__newInstance() const
{
    return new Router;
}
#ifdef __SUNPRO_CC
class ICE_API IceProxy::Ice::RouterFinder;
#endif
ICE_API ::IceProxy::Ice::Object* ::IceProxy::Ice::upCast(::IceProxy::Ice::RouterFinder* p) { return p; }

void
::IceProxy::Ice::__read(::IceInternal::BasicStream* __is, ::IceInternal::ProxyHandle< ::IceProxy::Ice::RouterFinder>& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::Ice::RouterFinder;
        v->__copyFrom(proxy);
    }
}

::Ice::RouterPrx
IceProxy::Ice::RouterFinder::getRouter(const ::Ice::Context* __ctx)
{
    __checkTwowayOnly(__Ice__RouterFinder__getRouter_name);
    ::IceInternal::Outgoing __og(this, __Ice__RouterFinder__getRouter_name, ::Ice::Normal, __ctx);
    __og.writeEmptyParams();
    if(!__og.invoke())
    {
        try
        {
            __og.throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
            throw __uue;
        }
    }
    ::Ice::RouterPrx __ret;
    ::IceInternal::BasicStream* __is = __og.startReadParams();
    __is->read(__ret);
    __og.endReadParams();
    return __ret;
}

::Ice::AsyncResultPtr
IceProxy::Ice::RouterFinder::begin_getRouter(const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    __checkAsyncTwowayOnly(__Ice__RouterFinder__getRouter_name);
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __Ice__RouterFinder__getRouter_name, __del, __cookie);
    try
    {
        __result->prepare(__Ice__RouterFinder__getRouter_name, ::Ice::Normal, __ctx);
        __result->writeEmptyParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

#ifdef ICE_CPP11

::Ice::AsyncResultPtr
IceProxy::Ice::RouterFinder::__begin_getRouter(const ::Ice::Context* __ctx, const ::IceInternal::Function<void (const ::Ice::RouterPrx&)>& __response, const ::IceInternal::Function<void (const ::Ice::Exception&)>& __exception, const ::IceInternal::Function<void (bool)>& __sent)
{
    class Cpp11CB : public ::IceInternal::Cpp11FnCallbackNC
    {
    public:

        Cpp11CB(const ::std::function<void (const ::Ice::RouterPrx&)>& responseFunc, const ::std::function<void (const ::Ice::Exception&)>& exceptionFunc, const ::std::function<void (bool)>& sentFunc) :
            ::IceInternal::Cpp11FnCallbackNC(exceptionFunc, sentFunc),
            _response(responseFunc)
        {
            CallbackBase::checkCallback(true, responseFunc || exceptionFunc != nullptr);
        }

        virtual void completed(const ::Ice::AsyncResultPtr& __result) const
        {
            ::Ice::RouterFinderPrx __proxy = ::Ice::RouterFinderPrx::uncheckedCast(__result->getProxy());
            ::Ice::RouterPrx __ret;
            try
            {
                __ret = __proxy->end_getRouter(__result);
            }
            catch(const ::Ice::Exception& ex)
            {
                Cpp11FnCallbackNC::exception(__result, ex);
                return;
            }
            if(_response != nullptr)
            {
                _response(__ret);
            }
        }
    
    private:
        
        ::std::function<void (const ::Ice::RouterPrx&)> _response;
    };
    return begin_getRouter(__ctx, new Cpp11CB(__response, __exception, __sent));
}
#endif

::Ice::RouterPrx
IceProxy::Ice::RouterFinder::end_getRouter(const ::Ice::AsyncResultPtr& __result)
{
    ::Ice::AsyncResult::__check(__result, this, __Ice__RouterFinder__getRouter_name);
    ::Ice::RouterPrx __ret;
    if(!__result->__wait())
    {
        try
        {
            __result->__throwUserException();
        }
        catch(const ::Ice::UserException& __ex)
        {
            throw ::Ice::UnknownUserException(__FILE__, __LINE__, __ex.ice_name());
        }
    }
    ::IceInternal::BasicStream* __is = __result->__startReadParams();
    __is->read(__ret);
    __result->__endReadParams();
    return __ret;
}

const ::std::string&
IceProxy::Ice::RouterFinder::ice_staticId()
{
    return ::Ice::RouterFinder::ice_staticId();
}

::IceProxy::Ice::Object*
IceProxy::Ice::RouterFinder::__newInstance() const
{
    return new RouterFinder;
}

ICE_API ::Ice::Object* Ice::upCast(::Ice::Router* p) { return p; }

namespace
{
const ::std::string __Ice__Router_ids[2] =
{
    "::Ice::Object",
    "::Ice::Router"
};

}

bool
Ice::Router::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__Ice__Router_ids, __Ice__Router_ids + 2, _s);
}

::std::vector< ::std::string>
Ice::Router::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__Ice__Router_ids[0], &__Ice__Router_ids[2]);
}

const ::std::string&
Ice::Router::ice_id(const ::Ice::Current&) const
{
    return __Ice__Router_ids[1];
}

const ::std::string&
Ice::Router::ice_staticId()
{
#ifdef ICE_HAS_THREAD_SAFE_LOCAL_STATIC
    static const ::std::string typeId = "::Ice::Router";
    return typeId;
#else
    return __Ice__Router_ids[1];
#endif
}

::Ice::DispatchStatus
Ice::Router::___getClientProxy(::IceInternal::Incoming& __inS, const ::Ice::Current& __current) const
{
    __checkMode(::Ice::Idempotent, __current.mode);
    __inS.readEmptyParams();
    ::Ice::ObjectPrx __ret = getClientProxy(__current);
    ::IceInternal::BasicStream* __os = __inS.__startWriteParams(::Ice::DefaultFormat);
    __os->write(__ret);
    __inS.__endWriteParams(true);
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
Ice::Router::___getServerProxy(::IceInternal::Incoming& __inS, const ::Ice::Current& __current) const
{
    __checkMode(::Ice::Idempotent, __current.mode);
    __inS.readEmptyParams();
    ::Ice::ObjectPrx __ret = getServerProxy(__current);
    ::IceInternal::BasicStream* __os = __inS.__startWriteParams(::Ice::DefaultFormat);
    __os->write(__ret);
    __inS.__endWriteParams(true);
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
Ice::Router::___addProxies(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.startReadParams();
    ::Ice::ObjectProxySeq __p_proxies;
    __is->read(__p_proxies);
    __inS.endReadParams();
    ::Ice::ObjectProxySeq __ret = addProxies(__p_proxies, __current);
    ::IceInternal::BasicStream* __os = __inS.__startWriteParams(::Ice::DefaultFormat);
    __os->write(__ret);
    __inS.__endWriteParams(true);
    return ::Ice::DispatchOK;
}

namespace
{
const ::std::string __Ice__Router_all[] =
{
    "addProxies",
    "getClientProxy",
    "getServerProxy",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

}

::Ice::DispatchStatus
Ice::Router::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< const ::std::string*, const ::std::string*> r = ::std::equal_range(__Ice__Router_all, __Ice__Router_all + 7, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __Ice__Router_all)
    {
        case 0:
        {
            return ___addProxies(in, current);
        }
        case 1:
        {
            return ___getClientProxy(in, current);
        }
        case 2:
        {
            return ___getServerProxy(in, current);
        }
        case 3:
        {
            return ___ice_id(in, current);
        }
        case 4:
        {
            return ___ice_ids(in, current);
        }
        case 5:
        {
            return ___ice_isA(in, current);
        }
        case 6:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
Ice::Router::__writeImpl(::IceInternal::BasicStream* __os) const
{
    __os->startWriteSlice(ice_staticId(), -1, true);
    __os->endWriteSlice();
}

void
Ice::Router::__readImpl(::IceInternal::BasicStream* __is)
{
    __is->startReadSlice();
    __is->endReadSlice();
}

void ICE_API 
Ice::__patch(RouterPtr& handle, const ::Ice::ObjectPtr& v)
{
    handle = ::Ice::RouterPtr::dynamicCast(v);
    if(v && !handle)
    {
        IceInternal::Ex::throwUOE(::Ice::Router::ice_staticId(), v);
    }
}

ICE_API ::Ice::Object* Ice::upCast(::Ice::RouterFinder* p) { return p; }

namespace
{
const ::std::string __Ice__RouterFinder_ids[2] =
{
    "::Ice::Object",
    "::Ice::RouterFinder"
};

}

bool
Ice::RouterFinder::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__Ice__RouterFinder_ids, __Ice__RouterFinder_ids + 2, _s);
}

::std::vector< ::std::string>
Ice::RouterFinder::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__Ice__RouterFinder_ids[0], &__Ice__RouterFinder_ids[2]);
}

const ::std::string&
Ice::RouterFinder::ice_id(const ::Ice::Current&) const
{
    return __Ice__RouterFinder_ids[1];
}

const ::std::string&
Ice::RouterFinder::ice_staticId()
{
#ifdef ICE_HAS_THREAD_SAFE_LOCAL_STATIC
    static const ::std::string typeId = "::Ice::RouterFinder";
    return typeId;
#else
    return __Ice__RouterFinder_ids[1];
#endif
}

::Ice::DispatchStatus
Ice::RouterFinder::___getRouter(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    __inS.readEmptyParams();
    ::Ice::RouterPrx __ret = getRouter(__current);
    ::IceInternal::BasicStream* __os = __inS.__startWriteParams(::Ice::DefaultFormat);
    __os->write(__ret);
    __inS.__endWriteParams(true);
    return ::Ice::DispatchOK;
}

namespace
{
const ::std::string __Ice__RouterFinder_all[] =
{
    "getRouter",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

}

::Ice::DispatchStatus
Ice::RouterFinder::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< const ::std::string*, const ::std::string*> r = ::std::equal_range(__Ice__RouterFinder_all, __Ice__RouterFinder_all + 5, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __Ice__RouterFinder_all)
    {
        case 0:
        {
            return ___getRouter(in, current);
        }
        case 1:
        {
            return ___ice_id(in, current);
        }
        case 2:
        {
            return ___ice_ids(in, current);
        }
        case 3:
        {
            return ___ice_isA(in, current);
        }
        case 4:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
Ice::RouterFinder::__writeImpl(::IceInternal::BasicStream* __os) const
{
    __os->startWriteSlice(ice_staticId(), -1, true);
    __os->endWriteSlice();
}

void
Ice::RouterFinder::__readImpl(::IceInternal::BasicStream* __is)
{
    __is->startReadSlice();
    __is->endReadSlice();
}

void ICE_API 
Ice::__patch(RouterFinderPtr& handle, const ::Ice::ObjectPtr& v)
{
    handle = ::Ice::RouterFinderPtr::dynamicCast(v);
    if(v && !handle)
    {
        IceInternal::Ex::throwUOE(::Ice::RouterFinder::ice_staticId(), v);
    }
}
