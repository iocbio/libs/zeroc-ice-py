// **********************************************************************
//
// Copyright (c) 2003-2018 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.6.5
//
// <auto-generated>
//
// Generated from file `IceDiscovery.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

#include <IceDiscovery/IceDiscovery.h>
#include <IceUtil/PushDisableWarnings.h>
#include <Ice/LocalException.h>
#include <Ice/ObjectFactory.h>
#include <Ice/Outgoing.h>
#include <Ice/OutgoingAsync.h>
#include <Ice/BasicStream.h>
#include <IceUtil/Iterator.h>
#include <IceUtil/PopDisableWarnings.h>

#ifndef ICE_IGNORE_VERSION
#   if ICE_INT_VERSION / 100 != 306
#       error Ice version mismatch!
#   endif
#   if ICE_INT_VERSION % 100 > 50
#       error Beta header file detected
#   endif
#   if ICE_INT_VERSION % 100 < 5
#       error Ice patch level mismatch!
#   endif
#endif

namespace
{

const ::std::string __IceDiscovery__LookupReply__foundObjectById_name = "foundObjectById";

const ::std::string __IceDiscovery__LookupReply__foundAdapterById_name = "foundAdapterById";

const ::std::string __IceDiscovery__Lookup__findObjectById_name = "findObjectById";

const ::std::string __IceDiscovery__Lookup__findAdapterById_name = "findAdapterById";

}
::IceProxy::Ice::Object* ::IceProxy::IceDiscovery::upCast(::IceProxy::IceDiscovery::LookupReply* p) { return p; }

void
::IceProxy::IceDiscovery::__read(::IceInternal::BasicStream* __is, ::IceInternal::ProxyHandle< ::IceProxy::IceDiscovery::LookupReply>& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::IceDiscovery::LookupReply;
        v->__copyFrom(proxy);
    }
}

void
IceProxy::IceDiscovery::LookupReply::foundObjectById(const ::Ice::Identity& __p_id, const ::Ice::ObjectPrx& __p_prx, const ::Ice::Context* __ctx)
{
    ::IceInternal::Outgoing __og(this, __IceDiscovery__LookupReply__foundObjectById_name, ::Ice::Normal, __ctx);
    try
    {
        ::IceInternal::BasicStream* __os = __og.startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_id);
        __os->write(__p_prx);
        __og.endWriteParams();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    __invoke(__og);
}

::Ice::AsyncResultPtr
IceProxy::IceDiscovery::LookupReply::begin_foundObjectById(const ::Ice::Identity& __p_id, const ::Ice::ObjectPrx& __p_prx, const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __IceDiscovery__LookupReply__foundObjectById_name, __del, __cookie);
    try
    {
        __result->prepare(__IceDiscovery__LookupReply__foundObjectById_name, ::Ice::Normal, __ctx);
        ::IceInternal::BasicStream* __os = __result->startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_id);
        __os->write(__p_prx);
        __result->endWriteParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

void
IceProxy::IceDiscovery::LookupReply::end_foundObjectById(const ::Ice::AsyncResultPtr& __result)
{
    __end(__result, __IceDiscovery__LookupReply__foundObjectById_name);
}

void
IceProxy::IceDiscovery::LookupReply::foundAdapterById(const ::std::string& __p_id, const ::Ice::ObjectPrx& __p_prx, bool __p_isReplicaGroup, const ::Ice::Context* __ctx)
{
    ::IceInternal::Outgoing __og(this, __IceDiscovery__LookupReply__foundAdapterById_name, ::Ice::Normal, __ctx);
    try
    {
        ::IceInternal::BasicStream* __os = __og.startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_id);
        __os->write(__p_prx);
        __os->write(__p_isReplicaGroup);
        __og.endWriteParams();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    __invoke(__og);
}

::Ice::AsyncResultPtr
IceProxy::IceDiscovery::LookupReply::begin_foundAdapterById(const ::std::string& __p_id, const ::Ice::ObjectPrx& __p_prx, bool __p_isReplicaGroup, const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __IceDiscovery__LookupReply__foundAdapterById_name, __del, __cookie);
    try
    {
        __result->prepare(__IceDiscovery__LookupReply__foundAdapterById_name, ::Ice::Normal, __ctx);
        ::IceInternal::BasicStream* __os = __result->startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_id);
        __os->write(__p_prx);
        __os->write(__p_isReplicaGroup);
        __result->endWriteParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

void
IceProxy::IceDiscovery::LookupReply::end_foundAdapterById(const ::Ice::AsyncResultPtr& __result)
{
    __end(__result, __IceDiscovery__LookupReply__foundAdapterById_name);
}

const ::std::string&
IceProxy::IceDiscovery::LookupReply::ice_staticId()
{
    return ::IceDiscovery::LookupReply::ice_staticId();
}

::IceProxy::Ice::Object*
IceProxy::IceDiscovery::LookupReply::__newInstance() const
{
    return new LookupReply;
}
::IceProxy::Ice::Object* ::IceProxy::IceDiscovery::upCast(::IceProxy::IceDiscovery::Lookup* p) { return p; }

void
::IceProxy::IceDiscovery::__read(::IceInternal::BasicStream* __is, ::IceInternal::ProxyHandle< ::IceProxy::IceDiscovery::Lookup>& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::IceDiscovery::Lookup;
        v->__copyFrom(proxy);
    }
}

void
IceProxy::IceDiscovery::Lookup::findObjectById(const ::std::string& __p_domainId, const ::Ice::Identity& __p_id, const ::IceDiscovery::LookupReplyPrx& __p_reply, const ::Ice::Context* __ctx)
{
    ::IceInternal::Outgoing __og(this, __IceDiscovery__Lookup__findObjectById_name, ::Ice::Idempotent, __ctx);
    try
    {
        ::IceInternal::BasicStream* __os = __og.startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_domainId);
        __os->write(__p_id);
        __os->write(__p_reply);
        __og.endWriteParams();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    __invoke(__og);
}

::Ice::AsyncResultPtr
IceProxy::IceDiscovery::Lookup::begin_findObjectById(const ::std::string& __p_domainId, const ::Ice::Identity& __p_id, const ::IceDiscovery::LookupReplyPrx& __p_reply, const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __IceDiscovery__Lookup__findObjectById_name, __del, __cookie);
    try
    {
        __result->prepare(__IceDiscovery__Lookup__findObjectById_name, ::Ice::Idempotent, __ctx);
        ::IceInternal::BasicStream* __os = __result->startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_domainId);
        __os->write(__p_id);
        __os->write(__p_reply);
        __result->endWriteParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

void
IceProxy::IceDiscovery::Lookup::end_findObjectById(const ::Ice::AsyncResultPtr& __result)
{
    __end(__result, __IceDiscovery__Lookup__findObjectById_name);
}

void
IceProxy::IceDiscovery::Lookup::findAdapterById(const ::std::string& __p_domainId, const ::std::string& __p_id, const ::IceDiscovery::LookupReplyPrx& __p_reply, const ::Ice::Context* __ctx)
{
    ::IceInternal::Outgoing __og(this, __IceDiscovery__Lookup__findAdapterById_name, ::Ice::Idempotent, __ctx);
    try
    {
        ::IceInternal::BasicStream* __os = __og.startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_domainId);
        __os->write(__p_id);
        __os->write(__p_reply);
        __og.endWriteParams();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    __invoke(__og);
}

::Ice::AsyncResultPtr
IceProxy::IceDiscovery::Lookup::begin_findAdapterById(const ::std::string& __p_domainId, const ::std::string& __p_id, const ::IceDiscovery::LookupReplyPrx& __p_reply, const ::Ice::Context* __ctx, const ::IceInternal::CallbackBasePtr& __del, const ::Ice::LocalObjectPtr& __cookie)
{
    ::IceInternal::OutgoingAsyncPtr __result = new ::IceInternal::OutgoingAsync(this, __IceDiscovery__Lookup__findAdapterById_name, __del, __cookie);
    try
    {
        __result->prepare(__IceDiscovery__Lookup__findAdapterById_name, ::Ice::Idempotent, __ctx);
        ::IceInternal::BasicStream* __os = __result->startWriteParams(::Ice::DefaultFormat);
        __os->write(__p_domainId);
        __os->write(__p_id);
        __os->write(__p_reply);
        __result->endWriteParams();
        __result->invoke();
    }
    catch(const ::Ice::Exception& __ex)
    {
        __result->abort(__ex);
    }
    return __result;
}

void
IceProxy::IceDiscovery::Lookup::end_findAdapterById(const ::Ice::AsyncResultPtr& __result)
{
    __end(__result, __IceDiscovery__Lookup__findAdapterById_name);
}

const ::std::string&
IceProxy::IceDiscovery::Lookup::ice_staticId()
{
    return ::IceDiscovery::Lookup::ice_staticId();
}

::IceProxy::Ice::Object*
IceProxy::IceDiscovery::Lookup::__newInstance() const
{
    return new Lookup;
}

::Ice::Object* IceDiscovery::upCast(::IceDiscovery::LookupReply* p) { return p; }

namespace
{
const ::std::string __IceDiscovery__LookupReply_ids[2] =
{
    "::Ice::Object",
    "::IceDiscovery::LookupReply"
};

}

bool
IceDiscovery::LookupReply::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__IceDiscovery__LookupReply_ids, __IceDiscovery__LookupReply_ids + 2, _s);
}

::std::vector< ::std::string>
IceDiscovery::LookupReply::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__IceDiscovery__LookupReply_ids[0], &__IceDiscovery__LookupReply_ids[2]);
}

const ::std::string&
IceDiscovery::LookupReply::ice_id(const ::Ice::Current&) const
{
    return __IceDiscovery__LookupReply_ids[1];
}

const ::std::string&
IceDiscovery::LookupReply::ice_staticId()
{
#ifdef ICE_HAS_THREAD_SAFE_LOCAL_STATIC
    static const ::std::string typeId = "::IceDiscovery::LookupReply";
    return typeId;
#else
    return __IceDiscovery__LookupReply_ids[1];
#endif
}

::Ice::DispatchStatus
IceDiscovery::LookupReply::___foundObjectById(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.startReadParams();
    ::Ice::Identity __p_id;
    ::Ice::ObjectPrx __p_prx;
    __is->read(__p_id);
    __is->read(__p_prx);
    __inS.endReadParams();
    foundObjectById(__p_id, __p_prx, __current);
    __inS.__writeEmptyParams();
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
IceDiscovery::LookupReply::___foundAdapterById(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.startReadParams();
    ::std::string __p_id;
    ::Ice::ObjectPrx __p_prx;
    bool __p_isReplicaGroup;
    __is->read(__p_id);
    __is->read(__p_prx);
    __is->read(__p_isReplicaGroup);
    __inS.endReadParams();
    foundAdapterById(__p_id, __p_prx, __p_isReplicaGroup, __current);
    __inS.__writeEmptyParams();
    return ::Ice::DispatchOK;
}

namespace
{
const ::std::string __IceDiscovery__LookupReply_all[] =
{
    "foundAdapterById",
    "foundObjectById",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

}

::Ice::DispatchStatus
IceDiscovery::LookupReply::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< const ::std::string*, const ::std::string*> r = ::std::equal_range(__IceDiscovery__LookupReply_all, __IceDiscovery__LookupReply_all + 6, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __IceDiscovery__LookupReply_all)
    {
        case 0:
        {
            return ___foundAdapterById(in, current);
        }
        case 1:
        {
            return ___foundObjectById(in, current);
        }
        case 2:
        {
            return ___ice_id(in, current);
        }
        case 3:
        {
            return ___ice_ids(in, current);
        }
        case 4:
        {
            return ___ice_isA(in, current);
        }
        case 5:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
IceDiscovery::LookupReply::__writeImpl(::IceInternal::BasicStream* __os) const
{
    __os->startWriteSlice(ice_staticId(), -1, true);
    __os->endWriteSlice();
}

void
IceDiscovery::LookupReply::__readImpl(::IceInternal::BasicStream* __is)
{
    __is->startReadSlice();
    __is->endReadSlice();
}

void 
IceDiscovery::__patch(LookupReplyPtr& handle, const ::Ice::ObjectPtr& v)
{
    handle = ::IceDiscovery::LookupReplyPtr::dynamicCast(v);
    if(v && !handle)
    {
        IceInternal::Ex::throwUOE(::IceDiscovery::LookupReply::ice_staticId(), v);
    }
}

::Ice::Object* IceDiscovery::upCast(::IceDiscovery::Lookup* p) { return p; }

namespace
{
const ::std::string __IceDiscovery__Lookup_ids[2] =
{
    "::Ice::Object",
    "::IceDiscovery::Lookup"
};

}

bool
IceDiscovery::Lookup::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__IceDiscovery__Lookup_ids, __IceDiscovery__Lookup_ids + 2, _s);
}

::std::vector< ::std::string>
IceDiscovery::Lookup::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__IceDiscovery__Lookup_ids[0], &__IceDiscovery__Lookup_ids[2]);
}

const ::std::string&
IceDiscovery::Lookup::ice_id(const ::Ice::Current&) const
{
    return __IceDiscovery__Lookup_ids[1];
}

const ::std::string&
IceDiscovery::Lookup::ice_staticId()
{
#ifdef ICE_HAS_THREAD_SAFE_LOCAL_STATIC
    static const ::std::string typeId = "::IceDiscovery::Lookup";
    return typeId;
#else
    return __IceDiscovery__Lookup_ids[1];
#endif
}

::Ice::DispatchStatus
IceDiscovery::Lookup::___findObjectById(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.startReadParams();
    ::std::string __p_domainId;
    ::Ice::Identity __p_id;
    ::IceDiscovery::LookupReplyPrx __p_reply;
    __is->read(__p_domainId);
    __is->read(__p_id);
    __is->read(__p_reply);
    __inS.endReadParams();
    findObjectById(__p_domainId, __p_id, __p_reply, __current);
    __inS.__writeEmptyParams();
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
IceDiscovery::Lookup::___findAdapterById(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.startReadParams();
    ::std::string __p_domainId;
    ::std::string __p_id;
    ::IceDiscovery::LookupReplyPrx __p_reply;
    __is->read(__p_domainId);
    __is->read(__p_id);
    __is->read(__p_reply);
    __inS.endReadParams();
    findAdapterById(__p_domainId, __p_id, __p_reply, __current);
    __inS.__writeEmptyParams();
    return ::Ice::DispatchOK;
}

namespace
{
const ::std::string __IceDiscovery__Lookup_all[] =
{
    "findAdapterById",
    "findObjectById",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

}

::Ice::DispatchStatus
IceDiscovery::Lookup::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< const ::std::string*, const ::std::string*> r = ::std::equal_range(__IceDiscovery__Lookup_all, __IceDiscovery__Lookup_all + 6, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __IceDiscovery__Lookup_all)
    {
        case 0:
        {
            return ___findAdapterById(in, current);
        }
        case 1:
        {
            return ___findObjectById(in, current);
        }
        case 2:
        {
            return ___ice_id(in, current);
        }
        case 3:
        {
            return ___ice_ids(in, current);
        }
        case 4:
        {
            return ___ice_isA(in, current);
        }
        case 5:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
IceDiscovery::Lookup::__writeImpl(::IceInternal::BasicStream* __os) const
{
    __os->startWriteSlice(ice_staticId(), -1, true);
    __os->endWriteSlice();
}

void
IceDiscovery::Lookup::__readImpl(::IceInternal::BasicStream* __is)
{
    __is->startReadSlice();
    __is->endReadSlice();
}

void 
IceDiscovery::__patch(LookupPtr& handle, const ::Ice::ObjectPtr& v)
{
    handle = ::IceDiscovery::LookupPtr::dynamicCast(v);
    if(v && !handle)
    {
        IceInternal::Ex::throwUOE(::IceDiscovery::Lookup::ice_staticId(), v);
    }
}
