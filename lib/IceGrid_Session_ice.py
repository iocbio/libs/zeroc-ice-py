# -*- coding: utf-8 -*-
# **********************************************************************
#
# Copyright (c) 2003-2018 ZeroC, Inc. All rights reserved.
#
# This copy of Ice is licensed to you under the terms described in the
# ICE_LICENSE file included in this distribution.
#
# **********************************************************************
#
# Ice version 3.6.5
#
# <auto-generated>
#
# Generated from file `Session.ice'
#
# Warning: do not edit this file.
#
# </auto-generated>
#

from sys import version_info as _version_info_
import Ice, IcePy
import Glacier2_Session_ice
import IceGrid_Exception_ice

# Included module Ice
_M_Ice = Ice.openModule('Ice')

# Included module Glacier2
_M_Glacier2 = Ice.openModule('Glacier2')

# Included module IceGrid
_M_IceGrid = Ice.openModule('IceGrid')

# Start of module IceGrid
__name__ = 'IceGrid'

if 'Session' not in _M_IceGrid.__dict__:
    _M_IceGrid.Session = Ice.createTempClass()
    class Session(_M_Glacier2.Session):
        """
        A session object is used by IceGrid clients to allocate and
        release objects. Client sessions are either created with the
        Registry object or the registry client Glacier2.SessionManager
        object.
        """
        def __init__(self):
            if Ice.getType(self) == _M_IceGrid.Session:
                raise RuntimeError('IceGrid.Session is an abstract class')

        def ice_ids(self, current=None):
            return ('::Glacier2::Session', '::Ice::Object', '::IceGrid::Session')

        def ice_id(self, current=None):
            return '::IceGrid::Session'

        def ice_staticId():
            return '::IceGrid::Session'
        ice_staticId = staticmethod(ice_staticId)

        def keepAlive(self, current=None):
            """
            Keep the session alive. Clients should call this operation
            regularly to prevent the server from reaping the session.
            Arguments:
            current -- The Current object for the invocation.
            """
            pass

        def allocateObjectById_async(self, _cb, id, current=None):
            """
            Allocate an object. Depending on the allocation timeout, this
            operation might hang until the object is available or until the
            timeout is reached.
            Arguments:
            _cb -- The asynchronous callback object.
            id -- The identity of the object to allocate.
            current -- The Current object for the invocation.
            Throws:
            AllocationException -- Raised if the object can't be allocated.
            ObjectNotRegisteredException -- Raised if the object with the given identity is not registered with the registry.
            """
            pass

        def allocateObjectByType_async(self, _cb, type, current=None):
            """
            Allocate an object with the given type. Depending on the
            allocation timeout, this operation can block until an object
            becomes available or until the timeout is reached.
            Arguments:
            _cb -- The asynchronous callback object.
            type -- The type of the object.
            current -- The Current object for the invocation.
            Throws:
            AllocationException -- Raised if the object could not be allocated.
            """
            pass

        def releaseObject(self, id, current=None):
            """
            Release an object that was allocated using allocateObjectById or
            allocateObjectByType.
            Arguments:
            id -- The identity of the object to release.
            current -- The Current object for the invocation.
            Throws:
            AllocationException -- Raised if the given object can't be released. This might happen if the object isn't allocatable or isn't allocated by the session.
            ObjectNotRegisteredException -- Raised if the object with the given identity is not registered with the registry.
            """
            pass

        def setAllocationTimeout(self, timeout, current=None):
            """
            Set the allocation timeout. If no objects are available for an
            allocation request, a call to allocateObjectById or
            allocateObjectByType will block for the duration of this
            timeout.
            Arguments:
            timeout -- The timeout in milliseconds.
            current -- The Current object for the invocation.
            """
            pass

        def __str__(self):
            return IcePy.stringify(self, _M_IceGrid._t_Session)

        __repr__ = __str__

    _M_IceGrid.SessionPrx = Ice.createTempClass()
    class SessionPrx(_M_Glacier2.SessionPrx):

        """
        Keep the session alive. Clients should call this operation
        regularly to prevent the server from reaping the session.
        Arguments:
        _ctx -- The request context for the invocation.
        """
        def keepAlive(self, _ctx=None):
            return _M_IceGrid.Session._op_keepAlive.invoke(self, ((), _ctx))

        """
        Keep the session alive. Clients should call this operation
        regularly to prevent the server from reaping the session.
        Arguments:
        _response -- The asynchronous response callback.
        _ex -- The asynchronous exception callback.
        _sent -- The asynchronous sent callback.
        _ctx -- The request context for the invocation.
        Returns: An asynchronous result object for the invocation.
        """
        def begin_keepAlive(self, _response=None, _ex=None, _sent=None, _ctx=None):
            return _M_IceGrid.Session._op_keepAlive.begin(self, ((), _response, _ex, _sent, _ctx))

        """
        Keep the session alive. Clients should call this operation
        regularly to prevent the server from reaping the session.
        Arguments:
        """
        def end_keepAlive(self, _r):
            return _M_IceGrid.Session._op_keepAlive.end(self, _r)

        """
        Allocate an object. Depending on the allocation timeout, this
        operation might hang until the object is available or until the
        timeout is reached.
        Arguments:
        id -- The identity of the object to allocate.
        _ctx -- The request context for the invocation.
        Returns: The proxy of the allocated object.
        Throws:
        AllocationException -- Raised if the object can't be allocated.
        ObjectNotRegisteredException -- Raised if the object with the given identity is not registered with the registry.
        """
        def allocateObjectById(self, id, _ctx=None):
            return _M_IceGrid.Session._op_allocateObjectById.invoke(self, ((id, ), _ctx))

        """
        Allocate an object. Depending on the allocation timeout, this
        operation might hang until the object is available or until the
        timeout is reached.
        Arguments:
        id -- The identity of the object to allocate.
        _response -- The asynchronous response callback.
        _ex -- The asynchronous exception callback.
        _sent -- The asynchronous sent callback.
        _ctx -- The request context for the invocation.
        Returns: An asynchronous result object for the invocation.
        """
        def begin_allocateObjectById(self, id, _response=None, _ex=None, _sent=None, _ctx=None):
            return _M_IceGrid.Session._op_allocateObjectById.begin(self, ((id, ), _response, _ex, _sent, _ctx))

        """
        Allocate an object. Depending on the allocation timeout, this
        operation might hang until the object is available or until the
        timeout is reached.
        Arguments:
        id -- The identity of the object to allocate.
        Returns: The proxy of the allocated object.
        Throws:
        AllocationException -- Raised if the object can't be allocated.
        ObjectNotRegisteredException -- Raised if the object with the given identity is not registered with the registry.
        """
        def end_allocateObjectById(self, _r):
            return _M_IceGrid.Session._op_allocateObjectById.end(self, _r)

        """
        Allocate an object with the given type. Depending on the
        allocation timeout, this operation can block until an object
        becomes available or until the timeout is reached.
        Arguments:
        type -- The type of the object.
        _ctx -- The request context for the invocation.
        Returns: The proxy of the allocated object.
        Throws:
        AllocationException -- Raised if the object could not be allocated.
        """
        def allocateObjectByType(self, type, _ctx=None):
            return _M_IceGrid.Session._op_allocateObjectByType.invoke(self, ((type, ), _ctx))

        """
        Allocate an object with the given type. Depending on the
        allocation timeout, this operation can block until an object
        becomes available or until the timeout is reached.
        Arguments:
        type -- The type of the object.
        _response -- The asynchronous response callback.
        _ex -- The asynchronous exception callback.
        _sent -- The asynchronous sent callback.
        _ctx -- The request context for the invocation.
        Returns: An asynchronous result object for the invocation.
        """
        def begin_allocateObjectByType(self, type, _response=None, _ex=None, _sent=None, _ctx=None):
            return _M_IceGrid.Session._op_allocateObjectByType.begin(self, ((type, ), _response, _ex, _sent, _ctx))

        """
        Allocate an object with the given type. Depending on the
        allocation timeout, this operation can block until an object
        becomes available or until the timeout is reached.
        Arguments:
        type -- The type of the object.
        Returns: The proxy of the allocated object.
        Throws:
        AllocationException -- Raised if the object could not be allocated.
        """
        def end_allocateObjectByType(self, _r):
            return _M_IceGrid.Session._op_allocateObjectByType.end(self, _r)

        """
        Release an object that was allocated using allocateObjectById or
        allocateObjectByType.
        Arguments:
        id -- The identity of the object to release.
        _ctx -- The request context for the invocation.
        Throws:
        AllocationException -- Raised if the given object can't be released. This might happen if the object isn't allocatable or isn't allocated by the session.
        ObjectNotRegisteredException -- Raised if the object with the given identity is not registered with the registry.
        """
        def releaseObject(self, id, _ctx=None):
            return _M_IceGrid.Session._op_releaseObject.invoke(self, ((id, ), _ctx))

        """
        Release an object that was allocated using allocateObjectById or
        allocateObjectByType.
        Arguments:
        id -- The identity of the object to release.
        _response -- The asynchronous response callback.
        _ex -- The asynchronous exception callback.
        _sent -- The asynchronous sent callback.
        _ctx -- The request context for the invocation.
        Returns: An asynchronous result object for the invocation.
        """
        def begin_releaseObject(self, id, _response=None, _ex=None, _sent=None, _ctx=None):
            return _M_IceGrid.Session._op_releaseObject.begin(self, ((id, ), _response, _ex, _sent, _ctx))

        """
        Release an object that was allocated using allocateObjectById or
        allocateObjectByType.
        Arguments:
        id -- The identity of the object to release.
        Throws:
        AllocationException -- Raised if the given object can't be released. This might happen if the object isn't allocatable or isn't allocated by the session.
        ObjectNotRegisteredException -- Raised if the object with the given identity is not registered with the registry.
        """
        def end_releaseObject(self, _r):
            return _M_IceGrid.Session._op_releaseObject.end(self, _r)

        """
        Set the allocation timeout. If no objects are available for an
        allocation request, a call to allocateObjectById or
        allocateObjectByType will block for the duration of this
        timeout.
        Arguments:
        timeout -- The timeout in milliseconds.
        _ctx -- The request context for the invocation.
        """
        def setAllocationTimeout(self, timeout, _ctx=None):
            return _M_IceGrid.Session._op_setAllocationTimeout.invoke(self, ((timeout, ), _ctx))

        """
        Set the allocation timeout. If no objects are available for an
        allocation request, a call to allocateObjectById or
        allocateObjectByType will block for the duration of this
        timeout.
        Arguments:
        timeout -- The timeout in milliseconds.
        _response -- The asynchronous response callback.
        _ex -- The asynchronous exception callback.
        _sent -- The asynchronous sent callback.
        _ctx -- The request context for the invocation.
        Returns: An asynchronous result object for the invocation.
        """
        def begin_setAllocationTimeout(self, timeout, _response=None, _ex=None, _sent=None, _ctx=None):
            return _M_IceGrid.Session._op_setAllocationTimeout.begin(self, ((timeout, ), _response, _ex, _sent, _ctx))

        """
        Set the allocation timeout. If no objects are available for an
        allocation request, a call to allocateObjectById or
        allocateObjectByType will block for the duration of this
        timeout.
        Arguments:
        timeout -- The timeout in milliseconds.
        """
        def end_setAllocationTimeout(self, _r):
            return _M_IceGrid.Session._op_setAllocationTimeout.end(self, _r)

        def checkedCast(proxy, facetOrCtx=None, _ctx=None):
            return _M_IceGrid.SessionPrx.ice_checkedCast(proxy, '::IceGrid::Session', facetOrCtx, _ctx)
        checkedCast = staticmethod(checkedCast)

        def uncheckedCast(proxy, facet=None):
            return _M_IceGrid.SessionPrx.ice_uncheckedCast(proxy, facet)
        uncheckedCast = staticmethod(uncheckedCast)

        def ice_staticId():
            return '::IceGrid::Session'
        ice_staticId = staticmethod(ice_staticId)

    _M_IceGrid._t_SessionPrx = IcePy.defineProxy('::IceGrid::Session', SessionPrx)

    _M_IceGrid._t_Session = IcePy.defineClass('::IceGrid::Session', Session, -1, (), True, False, None, (_M_Glacier2._t_Session,), ())
    Session._ice_type = _M_IceGrid._t_Session

    Session._op_keepAlive = IcePy.Operation('keepAlive', Ice.OperationMode.Idempotent, Ice.OperationMode.Idempotent, False, None, (), (), (), None, ())
    Session._op_allocateObjectById = IcePy.Operation('allocateObjectById', Ice.OperationMode.Normal, Ice.OperationMode.Normal, True, None, (), (((), _M_Ice._t_Identity, False, 0),), (), ((), IcePy._t_ObjectPrx, False, 0), (_M_IceGrid._t_ObjectNotRegisteredException, _M_IceGrid._t_AllocationException))
    Session._op_allocateObjectByType = IcePy.Operation('allocateObjectByType', Ice.OperationMode.Normal, Ice.OperationMode.Normal, True, None, (), (((), IcePy._t_string, False, 0),), (), ((), IcePy._t_ObjectPrx, False, 0), (_M_IceGrid._t_AllocationException,))
    Session._op_releaseObject = IcePy.Operation('releaseObject', Ice.OperationMode.Normal, Ice.OperationMode.Normal, False, None, (), (((), _M_Ice._t_Identity, False, 0),), (), None, (_M_IceGrid._t_ObjectNotRegisteredException, _M_IceGrid._t_AllocationException))
    Session._op_setAllocationTimeout = IcePy.Operation('setAllocationTimeout', Ice.OperationMode.Idempotent, Ice.OperationMode.Idempotent, False, None, (), (((), IcePy._t_int, False, 0),), (), None, ())

    _M_IceGrid.Session = Session
    del Session

    _M_IceGrid.SessionPrx = SessionPrx
    del SessionPrx

# End of module IceGrid

Ice.sliceChecksums["::IceGrid::Session"] = "cf4206d0a8aff6c1b0f2c437f34c5d"
